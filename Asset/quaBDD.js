KB.onClick('#gherkinButtonAdd', function(e) { gherkinButtonAddClicked(); });
KB.onClick('#emxampleButton', function(e) { gherkinButtonExampleClicked(); });
KB.onChange('#gherkinSteps', function(e) { $('#helpGherkin').html(changeHelpText($('#gherkinSteps').val(), $("html").attr('lang'))); });
KB.onClick('.gherkinLineCreatorDelete', function(e) { gherkinDeleteLine(e); });
KB.onClick('#exportAllGherkinsInFeatures', function(e) { downloadFeatureFile(); });
KB.onClick('#exportAllGherkinsInSteps', function(e) { downloadStepFile("stepFile" + $('#languageStepFile').val(), $('#implementationStepFile').val()); });

KB.onChange('#languageStepFile', function(e) { giveSelection($('#languageStepFile').val()); });
KB.onChange('#userStoryTemplateCheckbox', function(e) {
    console.log(document.getElementsByName('description')[0].value);
    if (document.getElementsByName('description')[0].value == '') {
        document.getElementsByName('description')[0].value = getTemplateText($("html").attr('lang'));
    } else if (document.getElementsByName('description')[0].value == getCheckTemplateText($("html").attr('lang'))) {
        document.getElementsByName('description')[0].value = '';
    }
});

function giveSelection(selValue) {
    var implementationStepFile = document.querySelector('#implementationStepFile');
    var options2 = implementationStepFile.querySelectorAll('option');

    for (var i = 0; i < options2.length; i++) {
        if (options2[i].dataset.option === selValue) {
            options2[i].setAttribute('style', 'display: block;');
            implementationStepFile.selectedIndex = i;
        } else {
            options2[i].setAttribute('style', 'display: none;');
        }
    }
}

function downloadStepFile(language, implementation) {
    document.getElementById(language + implementation).style.display = "block";
    var a = document.body.appendChild(
        document.createElement("a")
    );
    featureName = $(".showAllGherkins")[0].innerHTML.split('\n')[2].substr($(".showAllGherkins")[0].innerHTML.indexOf('Feature:') + 3).slice(0, -4);
    featureName = formatFeatureName(featureName);
    switch (language.slice(8)) {
        case ('Python'):
            a.download = featureName + "_step.py";
            break;
        case ('Java'):
            a.download = featureName + "_step.java";
            break;
        case ('JS'):
            a.download = featureName + "_step.js";
            break;
        case ('Ruby'):
            a.download = featureName + "_step.rb";
            break;
    }
    var doc = document;
    var element = $("#" + language + implementation)[0];
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
    a.href = "data:text/html," + selection;
    a.href = a.href.replace(/%C2%A0/g, ' ');
    a.click();
    clearSelection();
    document.getElementById(language + implementation).style.display = "none";
}

function downloadFeatureFile() {
    var a = document.body.appendChild(
        document.createElement("a")
    );
    featureName = $(".showAllGherkins")[0].innerHTML.split('\n')[2].substr($(".showAllGherkins")[0].innerHTML.indexOf('Feature:') + 3).slice(0, -4);
    featureName = formatFeatureName(featureName);
    a.download = featureName + ".feature";
    var doc = document;
    var element = $(".showAllGherkins")[0];
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
    a.href = "data:text/html," + selection;
    a.click();
    clearSelection();
}

function formatFeatureName(featureName) {
    featureName = featureName.toLowerCase();
    featureName = featureName.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    featureName = featureName.replace(/'/g, "_");
    featureName = featureName.replace(/ /g, "_");

    return featureName;
}

KB.onClick('#copyButtonAllGherkins', function(e) { $(".showAllGherkins").selectText(); });
jQuery.fn.selectText = function() {

    var doc = document;
    var element = this[0];
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }

    document.execCommand("copy");
    clearSelection();

    document.getElementById('copyGherkinAlert').style.display = 'block';
    $('#copyGherkinAlert').text('Text copied');
    setTimeout(function() {
        $('#copyGherkinAlert').text('');
        document.getElementById('copyGherkinAlert').style.display = 'none';
    }, 2000);
}

function clearSelection() {
    if (window.getSelection) { window.getSelection().removeAllRanges(); } else if (document.selection) { document.selection.empty(); }
}

function gherkinDeleteLine(e) {
    tr = e.path[1].closest('tr');
    table = e.path[1].closest('table');
    var nodes = Array.from(table.children[0].children);
    var index = nodes.indexOf(tr);
    var category = e.path[4].id.slice(0, -4);
    var container = document.getElementById(category + "GherkinContainer");
    container.removeChild(container.children[index]);
    tr.parentNode.removeChild(tr);
}

function gherkinButtonExampleClicked() {
    if (document.getElementById('exampleCaret').className == "fa fa-caret-down") {
        document.getElementById('exampleCaret').className = "fa fa-caret-up";
        document.getElementById('exampleContent').style.display = "flex";
    } else {
        document.getElementById('exampleCaret').className = "fa fa-caret-down";
        document.getElementById('exampleContent').style.display = "none";
    }
}

function gherkinButtonAddClicked() {

    if (document.getElementById('gherkinTextArea').value != "") {
        switch (document.getElementById('gherkinSteps').value) {
            case ('Given'):
                addStepToArray('given', document.getElementById('gherkinTextArea').value);
                printGherkinTolist('givenList', document.getElementById('gherkinTextArea').value);
                break;
            case ('When'):
                addStepToArray('when', document.getElementById('gherkinTextArea').value);
                printGherkinTolist('whenList', document.getElementById('gherkinTextArea').value);
                break;
            case ('Then'):
                addStepToArray('then', document.getElementById('gherkinTextArea').value);
                printGherkinTolist('thenList', document.getElementById('gherkinTextArea').value);
                break;
        }

        document.getElementById('gherkinTextArea').value = "";
    }
}

function addStepToArray(step, value) {
    var container = document.getElementById(step + "GherkinContainer");
    var input = document.createElement("input");
    input.type = "hidden";
    input.name = step + "[]";
    input.value = value;
    container.appendChild(input);
}

function printGherkinTolist(id, value) {
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    var th = document.createElement("th");
    var i = document.createElement("i");

    i.setAttribute("class", "fa fa-times gherkinLineCreatorDelete");
    td.setAttribute("class", "gherkinLine");

    td.appendChild(document.createTextNode(value));
    th.appendChild(i);
    tr.appendChild(td);
    tr.appendChild(th);

    document.getElementById(id).children[0].appendChild(tr);
}

function changeHelpText(value, language) {
    if (quaBDDTranslate[language] == null)
        return quaBDDTranslate['en'][value];

    return quaBDDTranslate[language][value];
}

function getTemplateText(language) {
    if (templateTextTranslate[language] == null)
        return templateTextTranslate['en'][0] + " \r\n" + templateTextTranslate['en'][1] + " \r\n" + templateTextTranslate['en'][2] + " \r\n" + templateTextTranslate['en'][3] + " ";

    return templateTextTranslate[language][0] + " \r\n" + templateTextTranslate[language][1] + " \r\n" + templateTextTranslate[language][2] + " \r\n" + templateTextTranslate[language][3] + " ";
}

function getCheckTemplateText(language) {
    if (templateTextTranslate[language] == null)
        return templateTextTranslate['en'][0] + " \n" + templateTextTranslate['en'][1] + " \n" + templateTextTranslate['en'][2] + " \n" + templateTextTranslate['en'][3] + " ";

    return templateTextTranslate[language][0] + " \n" + templateTextTranslate[language][1] + " \n" + templateTextTranslate[language][2] + " \n" + templateTextTranslate[language][3] + " ";
}