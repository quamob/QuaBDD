var quaBDDTranslate = {
    "fr": {
        "Given": "Décrivez le contexte initial. Cela décrit quelque chose qui s'est passé dans le passé",
        "When": "Décrivez un évènement, une action. Cela peut être une personne qui intéragit avec le système, ou ça peut être un évènement déclenché par un autre système",
        "Then": "Décrivez le résultat attendu après l'évènement"
    },
    "en": {
        "Given": "Describe the initial context of the system - the scene of the scenario. It is typically something that happened in the past.",
        "When": "Describe an event, or an action. This can be a person interacting with the system, or it can be an event triggered by another system.",
        "Then": "Describe an expected outcome, or result."
    }
};

var templateTextTranslate = {
    "fr": [
        "En tant que",
        "Lorsque",
        "Je",
        "Afin de"
    ],
    "en": [
        "As",
        "When",
        "I",
        "For"
    ],
};