<?php

namespace Kanboard\Plugin\QuaBDD\Controller;

use Kanboard\Controller\BaseController;
use Kanboard\Plugin\QuaBDD\Validator\TaskGherkinValidator;

/**
 * TaskGherkinController
 *
 * @package Kanboard\Plugin\QuaBDD\Controller
 * @author  Alexandre Turpin
 */
class TaskGherkinController extends BaseController
{
    /**
     * Creation form
     *
     * @access public
     * @param array $values
     * @param array $errors
     * @throws AccessForbiddenException
     * @throws PageNotFoundException
     */
    public function create(array $values = array(), array $errors = array())
    {
        $task = $this->getTask();
        $values['task_id'] = $task['id'];
        $values['edit'] = '0';
        
        $this->response->html($this->template->render('QuaBDD:board/createGherkinWindow', array(
            'values' => $values,
            'errors' => $errors,
            'task' => $task,
        )));
    }

    /**
     * Creation from another Gherkin form
     *
     * @access public
     * @param array $values
     * @param array $errors
     * @throws AccessForbiddenException
     * @throws PageNotFoundException
     */
    public function createFromAnotherGherkin(array $values = array(), array $errors = array())
    {
        $task = $this->getTask();
        $gherkin = $this->taskQuaBDDGherkinModel->getById($this->request->getIntegerParam('gherkin_id'));
        
        if(empty($values)){
            $values= $gherkin;
            $values['title'] = '';
            $values['given'] = json_decode($values['given']);
            $values['when'] = json_decode($values['when']);
            $values['then'] = json_decode($values['then']);
            $values['edit'] = '0';
        }
        
        $this->response->html($this->template->render('QuaBDD:board/createGherkinWindow', array(
            'values' => $values,
            'errors' => $errors,
            'task' => $task,
        )));
    }


    /**
     * Edit form
     *
     * @access public
     * @param array $values
     * @param array $errors
     * @throws AccessForbiddenException
     * @throws PageNotFoundException
     */
    public function edit(array $values = array(), array $errors = array())
    {
        $task = $this->getTask();
        $gherkin = $this->taskQuaBDDGherkinModel->getById($this->request->getIntegerParam('gherkin_id'));
        
        if(empty($values)){
            $values= $gherkin;
            $values['given'] = json_decode($values['given']);
            $values['when'] = json_decode($values['when']);
            $values['then'] = json_decode($values['then']);
            $values['edit'] = '1';
        }
        
        $this->response->html($this->template->render('QuaBDD:board/createGherkinWindow', array(
            'values' => $values,
            'errors' => $errors,
            'task' => $task,
        )));
    }

    /**
     * Validate and save a new gherkin
     *
     * @access public
     */
    public function save(){
        $taskGherkinValidator = new TaskGherkinValidator($this->container);

        $task = $this->getTask();
    
        $values = $this->request->getValues();
        $values['task_id'] = $task['id'];
        list($valid, $errors) = $taskGherkinValidator->validateCreation($values);

        if ($valid) {
            if($values['edit'] == '0')
            {
                if($this->createNewGherkinTask($values)){
                    $this->flash->success(t('Gherkin test added successfully.'));
                } 
                else {
                    $this->flash->failure(t('Unable to create your Gherkin test.'));
                }
            }
            else
            {
                if($this->editGherkin($values)){
                    $this->flash->success(t('Gherkin test edited successfully.'));
                } 
                else {
                    $this->flash->failure(t('Unable to edit your Gherkin test.'));
                }
            }

            if(isset($values['another_gherkin'])){
                $values['edit'] = 0;
                $this->create($values, $errors);
            }else{
                $this->response->redirect($this->helper->url->to('TaskViewController', 'show', array('task_id' => $task['id'], 'project_id' => $values['project_id'])), true);
            }
        }
        else{
            $this->create($values, $errors);
        }        
    }

    /**
     * Confirmation dialog before removing a gherkin
     *
     * @access public
     */
    public function confirm()
    {
        $task = $this->getTask();
        $gherkin = $this->taskQuaBDDGherkinModel->getById($this->request->getIntegerParam('gherkin_id'));

        $this->response->html($this->template->render('quaBDD:task_gherkin/remove', array(
            'gherkin' => $gherkin,
            'task' => $task,
            'title' => t('Remove a gherkin')
        )));
    }

    /**
     * Remove a gherkin
     *
     * @access public
     */
    public function remove()
    {
        $this->checkCSRFParam();
        $task = $this->getTask();
        $gherkin = $this->taskQuaBDDGherkinModel->getById($this->request->getIntegerParam('gherkin_id'));

        if ($this->taskQuaBDDGherkinModel->remove($gherkin['id'])) {
            $this->flash->success(t('Gherkin test removed successfully.'));
        } else {
            $this->flash->failure(t('Unable to remove this Gherkin test.'));
        }

        $this->response->redirect($this->helper->url->to('TaskViewController', 'show', array('task_id' => $task['id'], 'project_id' => $task['project_id'])), true);
    }

    /**
     * Encode arrays for the database then save all the values in the database
     * 
     * @access public
     */
    public function createNewGherkinTask($values){
        $taskGherkin = array(
            'task_id' => $values['task_id'],
            'title' => $values['title'],
            'given' => json_encode($values['given']),
            'when' => json_encode($values['when']),
            'then' => json_encode($values['then']),
        );

        $taskGherkin_id = $this->taskQuaBDDGherkinModel->create($taskGherkin);

        if(!$taskGherkin_id){
            $this->flash->failure(t('Unable to create this Gherkin test.'));
        }
        else{
            $this->flash->success(t('Gherkin test created successfully.'));
        }

        return $taskGherkin_id;
        
    }

    /**
     * Encode arrays for the database then save all the values for the gherkin that is modified in the database
     * 
     * @access public
     */
    public function editGherkin($values){
        $taskGherkin = array(
            'id' => $values['id'],
            'task_id' => $values['task_id'],
            'title' => $values['title'],
            'given' => json_encode($values['given']),
            'when' => json_encode($values['when']),
            'then' => json_encode($values['then']),
        );

        $taskGherkin_id = $this->taskQuaBDDGherkinModel->update($taskGherkin);

        if(!$taskGherkin_id){
            $this->flash->failure(t('Unable to edit this Gherkin test.'));
        }
        else{
            $this->flash->success(t('Gherkin test edited successfully.'));
        }

        return $taskGherkin_id;
        
    }

    /**
     * Show a gherkin
     * 
     * @access public
     */
    public function show()
    {
        $project = $this->getProject();
        $task = $this->getTask();

        $gherkins = $this->taskQuaBDDGherkinModel->getAllGherkinByTaskId($task['id']);

        $this->response->html($this->template->render('quaBDD:task_gherkin/table', array(
            'gherkin' => $gherkin,
            'task' => $task,
            'project' => $project,
        )));

    }

    /**
     * Show all the user stoy
     * 
     * @access public
     */
    public function showAll()
    {
        $project = $this->getProject();
        $task = $this->getTask();

        $gherkins = $this->taskQuaBDDGherkinModel->getAllGherkinByTaskId($task['id']);

        $this->response->html($this->template->render('quaBDD:task_gherkin/showAll', array(
            'gherkins' => $gherkins,
            'task' => $task,
            'project' => $project,
        )));

    }
}