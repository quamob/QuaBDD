<?php

namespace Kanboard\Plugin\QuaBDD\Validator;

use Kanboard\Validator\BaseValidator;
use SimpleValidator\Validator;
use SimpleValidator\Validators;
use Kanboard\Model\ProjectModel;

/**
 * TaskGherkinValidator
 *
 * @package Kanboard\Plugin\QuaBDD\Validator
 * @author  Alexandre Turpin
 */
class TaskGherkinValidator extends BaseValidator
{

    /**
     * Validate Gherkin creation
     *
     * @access public
     * @param  array   $values           Form values
     * @return array   $valid, $errors   [0] = Success or not, [1] = List of errors
     */
    public function validateCreation(array $values)
    {
        $rules = array(
            new Validators\Required('title', t('The gherkin title is required')),
        );

        $v = new Validator($values, $rules);

        return array(
            $v->execute(),
            $v->getErrors()
        );
    }

    /**
     * Validate Gherkin modification
     *
     * @access public
     * @param  array   $values           Form values
     * @return array   $valid, $errors   [0] = Success or not, [1] = List of errors
     */
    public function validateModification(array $values)
    {
        $rules = array(
            new Validators\Required('title', t('The gherkin title is required')),
        );

        $v = new Validator($values, $rules);

        return array(
            $v->execute(),
            $v->getErrors()
        );
    }
}
