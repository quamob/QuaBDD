<?php

namespace Kanboard\Plugin\QuaBDD;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

class Plugin extends Base
{
    public function initialize()
    {
        defined('QUABDD_SUBDIR_ENV') or define('QUABDD_SUBDIR_ENV', getenv('QUABDD_SUBDIR_ENV') ?: 'QuaBDD');

        //CSS
        $this->hook->on('template:layout:css', array('template' => 'plugins/QuaBDD/Asset/quaBDD.css'));
        
        //JS
        $this->hook->on('template:layout:js', array('template' => 'plugins/QuaBDD/Asset/translate.js'));
        $this->hook->on('template:layout:js', array('template' => 'plugins/QuaBDD/Asset/quaBDD.js'));

        //Template
        $this->template->hook->attach('template:task:sidebar:actions', 'quaBDD:board/createGherkinButton');
        $this->template->hook->attach('template:task:dropdown', 'quaBDD:board/createGherkinButton');
        $this->template->hook->attach('template:task:show:bottom', 'quaBDD:task_gherkin/table');
        $this->template->hook->attach('template:board:task:icons', 'quaBDD:board/gherkin_tag');
        $this->template->hook->attach('template:task:form:second-column', 'quaBDD:task_gherkin/userStoryCheckbox');

        //Model
        $this->container['taskQuaBDDGherkinModel'] = $this->container->factory(function ($c) {return new TaskQuaBDDGherkinModel ($c);});

        //Helper
        $this->helper->register('taskQuaBDDGherkinHelper', '\Kanboard\Plugin\QuaBDD\Helper\TaskQuaBDDGherkinHelper');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'QuaBDD';
    }

    public function getPluginDescription()
    {
        return t('This plugin allow you to store gherkin test directly your Kanboard tasks and give you an new interface that help you create Gherkin tests for a task. You will be able to export those gherkin into .feature and .step file.');
    }

    public function getPluginAuthor()
    {
        return 'Quamob';
    }

    public function getPluginVersion()
    {
        return '0.0.3';
    }

    public function getPluginHomepage()
    {
        return 'https://gitlab.com/quamob/QuaBDD';
    }
}

