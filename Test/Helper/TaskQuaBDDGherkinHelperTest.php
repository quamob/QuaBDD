<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaBDD/vendor/autoload.php';
require_once 'plugins/QuaBDD/Test/_helper/Fixtures.php';

use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaBDD\Plugin;
use Kanboard\Plugin\QuaBDD\Helper\TaskQuaBDDGherkinHelper;
use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

class TaskQuaBDDGherkinHelperTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function testGetAllGherkinsForTask()
    {
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getAll();

        $this->assertSame($result, $allGherkins);
    }
    
}
