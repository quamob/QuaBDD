<?php

require_once 'tests/units/Base.php';

use Kanboard\Plugin\QuaBDD\Plugin;

class PluginTest extends Base
{
    public function testPlugin()
    {
        $plugin = new Plugin($this->container);
        $this->assertSame(null, $plugin->initialize());
        $this->assertSame(null, $plugin->onStartup());
        $this->assertNotEmpty($plugin->getPluginName());
        $this->assertNotEmpty($plugin->getPluginDescription());
        $this->assertNotEmpty($plugin->getPluginAuthor());
        $this->assertNotEmpty($plugin->getPluginVersion());
        $this->assertNotEmpty($plugin->getPluginHomepage());
    }

    public function testAsset()
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();
        $this->assertTrue($plugin->hook->exists('template:layout:css'));
        $this->assertTrue($plugin->hook->exists('template:layout:js'));
    }

    public function testModel()
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();
        $this->assertNotEmpty($this->container['taskQuaBDDGherkinModel']);
    }

}
