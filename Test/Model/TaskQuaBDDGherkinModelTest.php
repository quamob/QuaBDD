<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaBDD/vendor/autoload.php';
require_once 'plugins/QuaBDD/Test/_helper/Fixtures.php';

use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaBDD\Plugin;
use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

class TaskQuaBDDGherkinModelTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function testCreateSuccess()
    {
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env = Fixtures::generateProjectAndTask($this->container);
        $values = Fixtures::prepareGherkin($env[1]);

        $this->assertNotFalse($taskQuaBDDGherkinModel->create($values));
    }

    public function testCreateWithNoTask()
    {
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $values = Fixtures::prepareGherkin(Fixtures::randomDataId());

        $this->assertFalse($taskQuaBDDGherkinModel->create($values));
    }

    public function testGetAllGherkinByTaskIdWithEmptyTable(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $result = $taskQuaBDDGherkinModel->getAllGherkinByTaskId(Fixtures::randomDataId());

        $this->assertSame($result, array());
    }

    public function testGetAllGherkinByTaskId(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $env2 = Fixtures::generateProjectAndTask($this->container);

        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $values4 = Fixtures::prepareGherkin($env2[1]);
        $values4Id = (string)$taskQuaBDDGherkinModel->create($values4);

        $result = $taskQuaBDDGherkinModel->getAllGherkinByTaskId($env1[1]);

        $this->assertSame($result, $allGherkins);
    }

    public function testGetAllGherkinByTaskIdStringParam(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $env2 = Fixtures::generateProjectAndTask($this->container);

        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $values4 = Fixtures::prepareGherkin($env2[1]);
        $values4Id = (string)$taskQuaBDDGherkinModel->create($values4);

        $result = $taskQuaBDDGherkinModel->getAllGherkinByTaskId(Fixtures::randomDataString());

        $this->assertSame($result, array());
    }

    public function testGetAllGherkinByTaskIdWrongId(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $env2 = Fixtures::generateProjectAndTask($this->container);

        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $values4 = Fixtures::prepareGherkin($env2[1]);
        $values4Id = (string)$taskQuaBDDGherkinModel->create($values4);

        $result = $taskQuaBDDGherkinModel->getAllGherkinByTaskId(Fixtures::randomDataLongId());

        $this->assertSame($result, array());
    }

    public function testGetById(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getById($allGherkins[0]['id']);

        $this->assertSame($result, $allGherkins[0]);
    }

    public function testGetByIdStringParam(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getById(Fixtures::randomDataString());

        $this->assertNull($result);
    }

    public function testGetByIdWrongId(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getById(Fixtures::randomDataLongId());

        $this->assertNull($result);
    }

    public function testGetAll(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getAll();

        $this->assertSame($result, $allGherkins);
    }

    public function testGetAllWithEmptyTable(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $result = $taskQuaBDDGherkinModel->getAll();

        $this->assertSame($result, array());
    }

    public function testGetAllByIds(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getAllByIds(array(1, 3));

        $this->assertSame($result, array($allGherkins[0], $allGherkins[2]));
    }

    public function testGetAllByIdsWithEmptyTable(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
        $faker = Faker\Factory::create();

        $result = $taskQuaBDDGherkinModel->getAllByIds(array(Fixtures::randomDataId(), Fixtures::randomDataId()));

        $this->assertSame($result, array());
    }

    public function testGetAllIds(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env1 = Fixtures::generateProjectAndTask($this->container);
        $allGherkins = Fixtures::generateGherkin(3, $env1[1], $this->container);

        $result = $taskQuaBDDGherkinModel->getAllIds();

        $this->assertSame($result, array($allGherkins[0]['id'], $allGherkins[1]['id'], $allGherkins[2]['id']));
    }

    public function testGetAllIdsWithEmptyTable(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $result = $taskQuaBDDGherkinModel->getAllIds();

        $this->assertSame($result, array());
    }

    public function testRemove(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env = Fixtures::generateProjectAndTask($this->container);
        $values1 = Fixtures::prepareGherkin($env[1]);
        $values2 = Fixtures::prepareGherkin($env[1]);
        $values3 = Fixtures::prepareGherkin($env[1]);

        $gherkin_id1 = $taskQuaBDDGherkinModel->create($values1);
        $gherkin_id2 = $taskQuaBDDGherkinModel->create($values2);
        $gherkin_id3 = $taskQuaBDDGherkinModel->create($values3);

        $this->assertSame(3, count($taskQuaBDDGherkinModel->getAll()));
        $taskQuaBDDGherkinModel->remove($gherkin_id2);
        $this->assertSame(2, count($taskQuaBDDGherkinModel->getAll()));
    }

    public function testEdit(){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);

        $env = Fixtures::generateProjectAndTask($this->container);
        $values = Fixtures::prepareGherkin($env[1]);

        $gherkin_id = $taskQuaBDDGherkinModel->create($values);
        $this->assertSame(1, count($taskQuaBDDGherkinModel->getAll()));

        $newValues['id'] = (string)$gherkin_id;
        $newValues['task_id'] = $values['task_id'];
        $newValues['title'] = 'title';
        $newValues['given'] = json_encode(array('given1', 'given2', 'given3'));
        $newValues['when'] = json_encode(array('when1', 'when2', 'when3'));
        $newValues['then'] = json_encode(array('then1', 'then2', 'then3'));

        $taskQuaBDDGherkinModel->update($newValues);
        $this->assertSame($newValues, $taskQuaBDDGherkinModel->getById($gherkin_id));
    }

}
