<?php

require_once 'plugins/QuaBDD/vendor/autoload.php';

use Kanboard\Model\ProjectModel;
use Kanboard\Model\TaskCreationModel;
use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

class Fixtures {

    public static function randomDataLongId()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify('[1-9]{2,255}');
    }

    public static function randomDataId()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify('[0-9]{1,5}');
    }

    public static function randomDataString()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify("[a-zA-Z._%+-@!?]{1,25}");
    }

    public static function randomDataLongString()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify("[a-zA-Z0-9._%+-@!?]{300,315}");
    }

    public static function generateProjectAndTask($container){
        $projectModel = new ProjectModel($container);
        $taskModel = new TaskCreationModel($container);

        $project_id = $projectModel->create(array('name' => 'Unit Test Project'));
        $task_id = $taskModel->create(array('title' => 'Unit Test Project', 'project_id' => $project_id));

        return array($project_id, $task_id);
    }

    public static function generateGherkin($numberOfGherkins, $task_id, $container){
        $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($container);

        $result = array();

        for($i=0; $i<$numberOfGherkins;$i++){
            $values = Fixtures::prepareGherkin($task_id);
            $id = $taskQuaBDDGherkinModel->create($values);
            array_push($result, ['id' => (string)$id] + $values);
        }

        return $result;
    }

    public static function prepareGherkin($task_id){
        $faker = Faker\Factory::create();

        $values['task_id'] = (string)$task_id;
        $values['title'] = Fixtures::randomDataString();
        $values['given'] = json_encode(array(Fixtures::randomDataString(), Fixtures::randomDataString()));
        $values['when'] = json_encode(array(Fixtures::randomDataString(), Fixtures::randomDataString()));
        $values['then'] = json_encode(array(Fixtures::randomDataString(), Fixtures::randomDataString()));

        return $values;
    }

}