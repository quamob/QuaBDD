<?php

namespace Kanboard\Plugin\QuaBDD\Helper;

use Kanboard\Core\Base;

/**
 * TaskQuaBDDGherkinHelper
 *
 * @package Kanboard\Plugin\QuaBDD\Helper
 * @author  Alexandre Turpin
 */
class TaskQuaBDDGherkinHelper extends Base
{
    /**
     * Return all gherkin assign to a task
     *
     * @access public
     * @param integer $task_id
     * @return array
     */
    public function getAllGherkinsForTask($task_id)
    {
        return $this->taskQuaBDDGherkinModel->getAllGherkinByTaskId($task_id); 
    }

    /**
     * Return the number of gherkin assign to a task
     *
     * @access public
     * @param integer $task_id
     * @return integer
     */
    public function getNumberOfGherkinsForTask($task_id)
    {
        return count($this->taskQuaBDDGherkinModel->getAllGherkinByTaskId($task_id)); 
    }
}