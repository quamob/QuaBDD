<?php

namespace Kanboard\Plugin\QuaBDD\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE `quaBDD_task_gherkin` (
            'id' INTEGER PRIMARY KEY,
            'task_id' INTEGER NOT NULL,
            'title' TEXT NOT NULL,
            'given' TEXT NOT NULL,
            'when' TEXT NOT NULL,
            'then' TEXT NOT NULL,
            FOREIGN KEY(task_id) REFERENCES tasks(id) ON DELETE CASCADE
        )
    ");
}