<?php

namespace Kanboard\Plugin\QuaBDD\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE `quaBDD_task_gherkin` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `task_id` INT NOT NULL,
            `title` TEXT NOT NULL,
            `given` VARCHAR(255) NOT NULL,
            `when` VARCHAR(255) NOT NULL,
            `then` VARCHAR(255) NOT NULL,
            FOREIGN KEY(task_id) REFERENCES tasks(id) ON DELETE CASCADE,
            PRIMARY KEY(id)
        )
    ");
}