QuaBDD
==============================

This plugin allow you to store gherkin test directly your Kanboard tasks and give you an new interface that help you create Gherkin tests for a task. You will be able to export those gherkin into .feature and .step file.

Author
------

- Alexandre Turpin
- Quamob
- License MIT

Requirements
------------

- Kanboard >= 1.2.14

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/QuaBDD`
3. Clone this repository into the folder `plugins/QuaBDD`

Note: Plugin folder is case-sensitive.
