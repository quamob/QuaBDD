<?php

namespace Kanboard\Plugin\QuaBDD\Model;

use Kanboard\Core\Base;

/**
 * TaskQuaBDDGherkinModel
 *
 * @package  Kanboard\Plugin\QuaBDD\Model
 * @author   Alexandre Turpin
 */
class TaskQuaBDDGherkinModel extends Base
{
    /**
     * SQL table name
     *
     * @var string
     */
    const TABLE = 'quaBDD_task_gherkin';

    /**
     * Get all gherkins by the task id
     *
     * @access public
     * @param  integer   $project_id    Project id
     * @return array
     */
    public function getAllGherkinByTaskId($task_id)
    {
        return $this->db->table(self::TABLE)->eq('task_id', $task_id)->findAll();
    }

    /**
     * Get a gherkin by id
     *
     * @access public
     * @param  integer   $project_id    Project id
     * @return array
     */
    public function getById($id)
    {
        return $this->db->table(self::TABLE)->eq('id', $id)->findOne();
    }

    /**
     * Get all gherkins
     *
     * @access public
     * @return array
     */
    public function getAll()
    {
        return $this->db->table(self::TABLE)->findAll();
    }

    /**
     * Get all gherkins with given Ids
     *
     * @access public
     * @param  integer[]   $ids
     * @return array
     */
    public function getAllByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        return $this->db->table(self::TABLE)->in('id', $ids)->findAll();
    }

    /**
     * Get all gherkins ids
     *
     * @access public
     * @return array
     */
    public function getAllIds()
    {
        return $this->db->table(self::TABLE)->findAllByColumn('id');
    }

    /**
     * Create a gherkin
     *
     * @access public
     * @param  array   $values Form values
     * @return int     Project id
     */
    public function create(array $values)
    {
        $this->db->startTransaction();

        if (! $this->db->table(self::TABLE)->save($values)) {
            $this->db->cancelTransaction();
            return false;
        }

        $projectQuaBDD_id = $this->db->getLastId();

        $this->db->closeTransaction();

        return (int) $projectQuaBDD_id;
    }

    /**
     * Remove a gherkin
     *
     * @access public
     * @param  integer  $gherkin_id  Comment id
     * @return boolean
     */
    public function remove($gherkin_id)
    {
        return $this->db->table(self::TABLE)->eq('id', $gherkin_id)->remove();
    }

     /**
     * Update a gherkin in the database
     *
     * @access public
     * @param  array    $values   Form values
     * @return boolean
     */
    public function update(array $values)
    {
        $result = $this->db
                    ->table(self::TABLE)
                    ->eq('id', $values['id'])
                    ->update($values);

        return $result;
    }

}