<?php $nbGherkins = $this->taskQuaBDDGherkinHelper->getNumberOfGherkinsForTask($task['id']); ?>
<?php if ($nbGherkins > 0): ?>
    <?php if ($not_editable): ?>
                <span title="<?= $nbGherkins == 1 ? t('%d Gherkin test', $nbGherkins) : t('%d Gherkin tests', $nbGherkins) ?>"><i class="fa fa-flask"></i>&nbsp;<?= $this->taskQuaBDDGherkinHelper->getNumberOfGherkinsForTask($task['id']) ?></span>
    <?php else: ?>
                <?= $this->modal->medium(
                    'flask',
                    $nbGherkins,
                    'TaskGherkinController',
                    'show',
                    array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'QuaBDD'),
                    $nbGherkins == 1 ? t('%d Gherkin test', $nbGherkins) : t('%d Gherkin tests', $nbGherkins)
                ) ?>
    <?php endif ?>
<?php endif ?>