<div class="page-header">
    <h2><?= $this->text->e(t('Gherkin test creator')) ?></h2>
</div>

<form method="post" action="<?= $this->url->href('TaskGherkinController', 'save', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'QuaBDD')) ?>" autocomplete="off">
    
    <?= $this->form->csrf() ?>

    <div class="gherkinTitle">
        <?= $this->form->label(t('Gherkin test title'), 'title') ?>
        <?= $this->form->text('title', $values, $errors, array('autofocus', 'required')) ?>
    </div>

    <div id="gherkinContainer">
        <div id="givenGherkinContainer">
            <?php if(isset($values['given'])) : ?>
                <?php foreach($values['given'] as $given):?>
                    <input type="hidden" name="given[]" value="<?= $given ?>">
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div id="whenGherkinContainer">
            <?php if(isset($values['when'])) : ?>
                <?php foreach($values['when'] as $when):?>
                    <input type="hidden" name="when[]" value="<?= $when ?>">
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div id="thenGherkinContainer">
            <?php if(isset($values['then'])) : ?>
                <?php foreach($values['then'] as $then):?>
                    <input type="hidden" name="then[]" value="<?= $then ?>">
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>

    <input type="hidden" name="edit" value="<?= $values['edit'] ?>">
    <input type="hidden" name="id" value="<?= isset($values['id']) ? $values['id'] : '0' ?>">

    <div class="gherkinContent"> 

        <div class="content">

            <div class="gherkinSelector">

                <select id="gherkinSteps" class="gherkinSelect" tabindex="16">
                    <option value="Given" selected="selected"><?= t('Decribe the initial context (Given)') ?></option>
                    <option value="When"><?= t('Describe the event (When)') ?></option>
                    <option value="Then"><?= t('Describe the expected outcome (Then)') ?></option> 
                </select>

                <p id="helpGherkin"><?= t('Describe the initial context of the system - the scene of the scenario. It is typically something that happened in the past.') ?></p>

                <textarea id="gherkinTextArea" tabindex="16" placeholder=""></textarea>

                <button id="gherkinButtonAdd" name="gherkinButtonAdd" type = "button"><?= t('ADD') ?></button>

            </div>

            <div id="gherkinResultQuestion">
                <h4><?= t('Decribe the initial context (Given)') ?> :</h4>

                <table id="givenList" class="gherkinCreatorTables">
                    <tbody>
                        <?php if(isset($values['given'])) : ?>
                            <?php foreach($values['given'] as $given):?>
                                <tr>
                                    <td class="gherkinLine"><?= $given ?></td>
                                    <th><i class="fa fa-times gherkinLineCreatorDelete"></i></th>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>

                <h4><?= t('Describe the event (When)') ?> :</h4>
                <table id="whenList" class="gherkinCreatorTables">
                    <tbody>
                        <?php if(isset($values['when'])) : ?>
                            <?php foreach($values['when'] as $when):?>
                                <tr>
                                    <td class="gherkinLine"><?= $when ?></td>
                                    <th><i class="fa fa-times gherkinLineCreatorDelete"></i></th>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <h4><?= t('Describe the expected outcome (Then)') ?> :</h4>
                <table id="thenList" class="gherkinCreatorTables">
                    <tbody>
                        <?php if(isset($values['then'])) : ?>
                            <?php foreach($values['then'] as $then):?>
                                <tr>
                                    <td class="gherkinLine"><?= $then ?></td>
                                    <th><i class="fa fa-times gherkinLineCreatorDelete"></i></th>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

    <div class="example">
        <a id="emxampleButton" href="#" class="dropdown-menu-link-icon dropdown-menu">
            <?= t('Example') ?>
            <i id="exampleCaret" class="fa fa-caret-down"></i>
        </a>

        <div id="exampleContent">
            <div id="gherkinResultQuestion">
                <h4><?= t('Decribe the initial context (Given)') ?> :</h3>
                <ul>
                    <li><?= t('There are 20 cucumbers') ?></li>
                    <li><?= t('There are 10 apples') ?></li>
                </ul>
                <h4><?= t('Describe the event (When)') ?> :</h3>
                <ul>
                    <li><?= t('I eat 5 cucumbers') ?></li>
                    <li><?= t('I eat 2 apples') ?></li>
                </ul>
                <h4><?= t('Describe the expected outcome (Then)') ?> :</h3>
                <ul>
                    <li><?= t('I should have 15 cucumbers') ?></li>
                    <li><?= t('I should have 8 apples') ?></li>
                </ul>
            </div>
        </div>
    </div>
    
    <?= $this->form->checkbox('another_gherkin', t('Create another gherkin'), 1, 0) ?>

    <?= $this->modal->submitButtons() ?>
    
</form>