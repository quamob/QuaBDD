<div class="page-header">
    <h2><?= t('Remove a Gherkin test') ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Do you really want to remove this Gherkin test ?') ?>
    </p>

    <?= $this->render('quaBDD:task_gherkin/show', array(
        'gherkin' => $gherkin,
        'task' => $task,
        'hide_actions' => true
    )) ?>

    <?= $this->modal->confirmButtons(
        'TaskGherkinController',
        'remove',
        array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'gherkin_id' => $gherkin['id'], 'plugin' => 'QuaBDD')
    ) ?>
</div>
