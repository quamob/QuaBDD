<div class="allGherkinActions">
    <a id="exportAllGherkinsInFeatures" class="exportAllGherkins"><i class="fa fa-download"></i> <?= t('Export .feature file') ?></a>
    <a id="exportAllGherkinsInSteps" class="exportAllGherkins"><i class="fa fa-download"></i> <?= t('Export .step file') ?></a>
    
    <div class="selectorStepFile">
        <select name="language" id="languageStepFile">
            <option value="Python">Python</option>
            <option value="Java">Java</option>
            <option value="JS">JS</option>
            <option value="Ruby">Ruby</option>
        </select>

        <select name="implementation" id="implementationStepFile">
            <option data-option="Python" value="Behave">Behave</option>
            <option data-option="Java" value="Cucumber-JVM" style="display: none;">Cucumber-JVM</option>
            <option data-option="JS" value="CucumberJS" style="display: none;">Cucumber.JS</option>
            <option data-option="Ruby" value="CucumberRB" style="display: none;">Cucumber.RB</option>
        </select>
    </div>

    <a id="copyButtonAllGherkins"><i class="fa fa-clipboard"></i> <?= t('Copy') ?></a>
    <p id="copyGherkinAlert"></p>
</div>

<div class="showAllGherkins">
    
        Feature: <?= $task['title'] ?><br>
        &nbsp;&nbsp;&nbsp;&nbsp;<?= str_replace("\r\n", "<br>&nbsp;&nbsp;&nbsp;&nbsp;", $task['description']); ?>

    <?php foreach($gherkins as $gherkin): ?>
        <?php 
            $givens = json_decode($gherkin['given']);
            $whens = json_decode($gherkin['when']); 
            $thens = json_decode($gherkin['then']);
        ?>
        <br>
        <br>
            &nbsp;&nbsp;&nbsp;&nbsp;Scenario: <?= $gherkin['title'] ?>
        
        <br>
            <?php if(isset($givens[0])): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Given <?= $givens[0] ?>
                <?php unset($givens[0]);?>
            <?php endif ?>
            <?php foreach($givens as $given): ?>
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;And <?= $given ?>                 
            <?php endforeach ?>
        

        <br>
            <?php if(isset($whens[0])): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;When <?= $whens[0] ?> 
                <?php unset($whens[0]);?>
            <?php endif ?>
            <?php foreach($whens as $when): ?>
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;And <?= $when ?>                
            <?php endforeach ?>
        

        <br>
            <?php if(isset($thens[0])): ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Then <?= $thens[0] ?> 
                <?php unset($thens[0]);?>
            <?php endif ?>
            <?php foreach($thens as $then): ?>
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;And <?= $then ?>                
            <?php endforeach ?>
        
    <?php endforeach ?>

</div>

<div id="stepFilePythonBehave">

    from behave import *
    <?php
        $givens = array();
        $whens = array();
        $thens = array();
    ?>
    <?php foreach($gherkins as $gherkin): ?>
        <?php 
            $givenGherkins = json_decode($gherkin['given']);
            $whensGherkins = json_decode($gherkin['when']); 
            $thensGherkins = json_decode($gherkin['then']);

            foreach($givenGherkins as $givenGherkin){
                $givens[] = $givenGherkin;
            }

            foreach($whensGherkins as $whensGherkin){
                $whens[] = $whensGherkin;
            }

            foreach($thensGherkins as $thensGherkin){
                $thens[] = $thensGherkin;
            }

            $givens = array_unique($givens);
            $whens = array_unique($whens);
            $thens = array_unique($thens);
        ?>

        <?php endforeach ?>
        <br>
        <br>
        <?php foreach($givens as $given): ?>
            
            <br>@Given("<?= $given ?>")
            <br>def step_impl(context):
            <br>&nbsp;&nbsp;raise NotImplementedError("STEP: Given <?= $given ?>")
            <br>
        <?php endforeach ?>
 
        <?php foreach($whens as $when): ?>
            
            <br>@When("<?= $when ?>")
            <br>def step_impl(context):
            <br>&nbsp;&nbsp;raise NotImplementedError("STEP: When <?= $when ?>")
            <br>
        <?php endforeach ?>

        <?php foreach($thens as $then): ?>
            
            <br>@Then("<?= $then ?>")
            <br>def step_impl(context):
            <br>&nbsp;&nbsp;raise NotImplementedError("STEP: Then <?= $then ?>")
            <br>
        <?php endforeach ?>

</div>

<div id="stepFileJavaCucumber-JVM">

    public class StepDefinitions { 
    <?php
        $givens = array();
        $whens = array();
        $thens = array();
    ?>
    <?php foreach($gherkins as $gherkin): ?>
        <?php 
            $givenGherkins = json_decode($gherkin['given']);
            $whensGherkins = json_decode($gherkin['when']); 
            $thensGherkins = json_decode($gherkin['then']);

            foreach($givenGherkins as $givenGherkin){
                $givens[] = $givenGherkin;
            }

            foreach($whensGherkins as $whensGherkin){
                $whens[] = $whensGherkin;
            }

            foreach($thensGherkins as $thensGherkin){
                $thens[] = $thensGherkin;
            }

            $givens = array_unique($givens);
            $whens = array_unique($whens);
            $thens = array_unique($thens);
        ?>
    <?php endforeach ?>
    <br>
    <br>
    <?php foreach($givens as $given): ?>
        
        <br>@Given("<?= $given ?>")
        <br>public void <?= formatStepName($given) ?>() throws Throwable {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;throw new PendingException();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;}
        <br>
    <?php endforeach ?>

    <?php foreach($whens as $when): ?>
        
        <br>@When("<?= $when ?>")
        <br>public void <?= formatStepName($when) ?>() throws Throwable {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;throw new PendingException();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;}
        <br>
    <?php endforeach ?>

    <?php foreach($thens as $then): ?>
        
        <br>@Then("<?= $then ?>")
        <br>public void <?= formatStepName($then) ?>() throws Throwable {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;throw new PendingException();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;}
        <br>
    <?php endforeach ?>
    
    <br>
    }

</div>

<div id="stepFileJSCucumberJS">

    'use strict';
    <br>
    <br>
    module.exports = function() {
    <?php
        $givens = array();
        $whens = array();
        $thens = array();
    ?>
    <?php foreach($gherkins as $gherkin): ?>
        <?php 
            $givenGherkins = json_decode($gherkin['given']);
            $whensGherkins = json_decode($gherkin['when']); 
            $thensGherkins = json_decode($gherkin['then']);

            foreach($givenGherkins as $givenGherkin){
                $givens[] = $givenGherkin;
            }

            foreach($whensGherkins as $whensGherkin){
                $whens[] = $whensGherkin;
            }

            foreach($thensGherkins as $thensGherkin){
                $thens[] = $thensGherkin;
            }

            $givens = array_unique($givens);
            $whens = array_unique($whens);
            $thens = array_unique($thens);
        ?>
    <?php endforeach ?>
    <br>
    <br>
    <?php foreach($givens as $given): ?>
        
        <br>this.Given(/^<?= $given ?>$/, function(callback) {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;callback.pending();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;});
        <br>
    <?php endforeach ?>

    <?php foreach($whens as $when): ?>
        
        <br>this.When(/^<?= $when ?>$/, function(callback) {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;callback.pending();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;});
        <br>
    <?php endforeach ?>

    <?php foreach($thens as $then): ?>
        
        <br>this.Then(/^<?= $then ?>$/, function(callback) {
        <br>&nbsp;&nbsp;&nbsp;&nbsp;callback.pending();
        <br>&nbsp;&nbsp;&nbsp;&nbsp;});
        <br>
    <?php endforeach ?>
    
    <br>
    };

</div>

<div id="stepFileRubyCucumberRB">

    <?php
        $givens = array();
        $whens = array();
        $thens = array();
    ?>
    <?php foreach($gherkins as $gherkin): ?>
        <?php 
            $givenGherkins = json_decode($gherkin['given']);
            $whensGherkins = json_decode($gherkin['when']); 
            $thensGherkins = json_decode($gherkin['then']);

            foreach($givenGherkins as $givenGherkin){
                $givens[] = $givenGherkin;
            }

            foreach($whensGherkins as $whensGherkin){
                $whens[] = $whensGherkin;
            }

            foreach($thensGherkins as $thensGherkin){
                $thens[] = $thensGherkin;
            }

            $givens = array_unique($givens);
            $whens = array_unique($whens);
            $thens = array_unique($thens);
        ?>
    <?php endforeach ?>

    <?php foreach($givens as $given): ?>
        <br>Given(/^<?= $given ?>$/) do
        <br>&nbsp;&nbsp;&nbsp;&nbsp;pending
        <br>end
        <br>
    <?php endforeach ?>

    <?php foreach($whens as $when): ?>
        
        <br>When(/^<?= $when ?>$/) do
        <br>&nbsp;&nbsp;&nbsp;&nbsp;pending
        <br>end
        <br>
    <?php endforeach ?>

    <?php foreach($thens as $then): ?>
        
        <br>Then(/^<?= $then ?>$/) do
        <br>&nbsp;&nbsp;&nbsp;&nbsp;pending
        <br>end
        <br>
    <?php endforeach ?>

</div>

<?php
    function formatStepName($stepName){
        $stepName = str_replace(" ", "_", $stepName);
        $stepName = str_replace("'", "_", $stepName);
        $stepName = strtolower($stepName);
        $stepName = remove_accents($stepName);
        return $stepName;
    }

    function remove_accents($string) {
        if ( !preg_match('/[\x80-\xff]/', $string) )
            return $string;
    
        $chars = array(
        // Decompositions for Latin-1 Supplement
        chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
        chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
        chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
        chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
        chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
        chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
        chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
        chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
        chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
        chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
        chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
        chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
        chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
        chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
        chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
        chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
        chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
        chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
        chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
        chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
        chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
        chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
        chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
        chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
        chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
        chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
        chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
        chr(195).chr(191) => 'y',
        // Decompositions for Latin Extended-A
        chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
        chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
        chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
        chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
        chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
        chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
        chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
        chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
        chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
        chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
        chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
        chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
        chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
        chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
        chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
        chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
        chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
        chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
        chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
        chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
        chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
        chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
        chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
        chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
        chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
        chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
        chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
        chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
        chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
        chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
        chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
        chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
        chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
        chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
        chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
        chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
        chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
        chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
        chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
        chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
        chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
        chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
        chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
        chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
        chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
        chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
        chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
        chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
        chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
        chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
        chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
        chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
        chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
        chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
        chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
        chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
        chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
        chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
        chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
        chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
        chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
        chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
        chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
        chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
        );
    
        $string = strtr($string, $chars);
    
        return $string;
    }


?>