<div class="gherkin" id="gherkin">
    <div class="gherkin-content">
        <div class="markdown">
            <?php 
            $givens =  json_decode($gherkin['given']);
            $whens =  json_decode($gherkin['when']);
            $thens =  json_decode($gherkin['then']);
            ?>

            <?php if (! isset($hide_actions)): ?>
                <details class="accordion-section-gherkin">
                        <summary class="accordion-title-gherkin"><?=  $gherkin['title'] ?></summary>
                        <div class="accordion-content-gherkin">
                            <div>
                                <?= 'Given ' . $givens[0]?>
                                <?php foreach(array_slice($givens,1) as $given): ?>
                                    </br>
                                    <?= 'And ' . $given?>
                                <?php endforeach ?>
                            </div>

                            <div>
                            <?= 'When ' . $whens[0]?>
                            <?php foreach(array_slice($whens,1) as $when): ?>
                                </br>
                                <?= 'And ' . $when?>
                            <?php endforeach ?>
                            </div>

                            <div>
                            <?= 'Then ' . $thens[0]?>
                            <?php foreach(array_slice($thens,1) as $then): ?>
                                </br>
                                <?= 'And ' . $then?>
                            <?php endforeach ?>
                            </div>

                        </div>
                </details>

            <?php else : ?>
                <?=  $gherkin['title'] ?>
                </br>
                </br>
                <div>
                    <?= 'Given ' . $givens[0]?>
                    <?php foreach(array_slice($givens,1) as $given): ?>
                        </br>
                        <?= 'And ' . $given?>
                    <?php endforeach ?>
                </div>

                <div>
                <?= 'When ' . $whens[0]?>
                <?php foreach(array_slice($whens,1) as $when): ?>
                    </br>
                    <?= 'And ' . $when?>
                <?php endforeach ?>
                </div>

                <div>
                <?= 'Then ' . $thens[0]?>
                <?php foreach(array_slice($thens,1) as $then): ?>
                    </br>
                    <?= 'And ' . $then?>
                <?php endforeach ?>
                </div>
            <?php endif ?>
        </div>
    </div>

    <div class="gherkinsActions">
         <?php if (!isset($hide_actions) && $editable): ?>
            <div class="dropdown">
                <?= $this->modal->medium('plus', '', 'TaskGherkinController', 'createFromAnotherGherkin', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'gherkin_id' => $gherkin['id'], 'plugin' => 'QuaBDD'), t('Create another gherkin from this gherkin')) ?>
            </div>
        <?php endif ?>

        <?php if (!isset($hide_actions) && $editable): ?>
            <div class="dropdown">
                <?= $this->modal->medium('edit', '', 'TaskGherkinController', 'edit', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'gherkin_id' => $gherkin['id'], 'plugin' => 'QuaBDD'), t('Edit')) ?>
            </div>
        <?php endif ?>

        <?php if (!isset($hide_actions) && $editable): ?>
            <div class="dropdown">
                <?= $this->modal->confirm('trash-o', '', 'TaskGherkinController', 'confirm', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'gherkin_id' => $gherkin['id'], 'plugin' => 'QuaBDD'), t('Remove')) ?>
            </div>
        <?php endif ?>
    </div>
</div>