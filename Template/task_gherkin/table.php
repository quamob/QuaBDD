<?php $gherkins = $this->taskQuaBDDGherkinHelper->getAllGherkinsForTask($task['id']); ?>

<details class="accordion-section" <?= empty($gherkins) ? '' : 'open' ?>>
    <summary class="accordion-title"><?= t('Gherkins') ?></summary>
    <div class="accordion-content">
        <div class="gherkins-actions-bar">
        <?php if($this->user->hasProjectAccess('CommentController', 'edit', $project['id'])) :?>
            <div class="add-gherkin-button">
                <?= $this->modal->medium('plus', t('Add Gherkin test'), 'TaskGherkinController', 'create', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'QuaBDD')) ?>
            </div>
        <?php endif ?>
            <div class="show-all-gherkin-button">
                <?= $this->modal->medium('eye', t('Display all Gherkin test'), 'TaskGherkinController', 'showAll', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'QuaBDD')) ?>
            </div>
        </div>
        <?php if (! empty($gherkins)): ?>
            <?php  foreach($gherkins as $gherkin): ?>
                <?= $this->render('quaBDD:task_gherkin/show', array(
                        'gherkin' => $gherkin,
                        'task' => $task,
                        'project' => $project,
                        'editable' => $this->user->hasProjectAccess('CommentController', 'edit', $project['id']),
                    )) ?>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</details>